#!/bin/bash

# this script uses two tools in tools/ to prepare a CoNLL version of the
# coreference corpus from the unpacked METU-Sabanci Treebank

TOOLDIR=tools-baseline

# extract XML documents from Treebank
python3 $TOOLDIR/extract-documents.py

# create CoNLL directory
mkdir -p conll

# convert all gold XML files to CoNLL
for doc in `ls -1 gold/`; do
  base=${doc/.xml/}
  echo $base
  PYTHONPATH=$TOOLDIR/ python3 $TOOLDIR/xml-to-conll.py documents/$base.xml gold/$base.xml $base conll/$base.conll
done

# the following is deactivated by default. it tests converting back from conll to xml
if /bin/false; then
  # convert all gold XML files to CoNLL
  mkdir -p testout
  for doc in `ls -1 gold/`; do
    base=${doc/.xml/}
    echo $base
    PYTHONPATH=$TOOLDIR/ python3 $TOOLDIR/conll-to-xml.py conll/$base.conll testout/$base.xml
  done
fi
