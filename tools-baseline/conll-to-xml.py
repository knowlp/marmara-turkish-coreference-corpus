#!/usr/bin/python
# -*- coding: utf8 -*-

# Converting CoNLL format to Marmara Turkish Coreference Corpus XML.
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# this script is used to take a conll file (without "=" annotatiosn)
# as produced in our adjudcation, and convert the last column into a XML
#

import sys
import collections

import xml.etree.ElementTree as ET

import knowlpcoref

def warn(s):
  sys.stderr.write(s+'\n')

def usage():
  print("usage: {} <document-file-in>.conll <out-file>.xml".format(sys.argv[0]))
  print("  will create/overwrite <out-file>.xml")

def main():
  inpfile = sys.argv[1]
  outprefix = sys.argv[2]
  with open(inpfile, 'r') as i:
    txt = i.read()
    rawconll = knowlpcoref.readCONLLRaw(txt)
  #warn('rawconll = '+repr(rawconll))

  if len(rawconll) != 1:
    raise Exception("found {} documents in input {}, need exactly one!".format(len(rawconll), inpfile))
  docname, lines = next(iter(rawconll.items()))
  #warn('lines = '+repr(lines))
  nlines = len(lines)
  assert(nlines > 0)
  ncols = len(lines[0])
  warn('found {} lines with {} colums in document {}'.format(nlines, ncols, inpfile))
  
  # use first four columns and last one (TODO make this configurable)
  interpreted = knowlpcoref.interpretCoNLLRaw(lines, [0,1,2,3,ncols-1], ncols-1, onlyUser=False)
  #warn('interpreted '+repr(interpreted))

  # write out XML file
  xmloutfile = outprefix
  warn('writing {}'.format(xmloutfile))
  with open(xmloutfile, 'w') as of:
    of.write(knowlpcoref.toXML(docname, interpreted))

if __name__ == '__main__':
  if len(sys.argv) != 3:
    usage()
  else:
    main()
