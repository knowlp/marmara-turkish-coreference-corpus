#!/bin/bash

# before running this script, extract tb_corrected.zip to ../ and run create-conll-documents.sh there
TBC_DIR="../documents"
CRC_DIR="../gold"

# all documents
DOCS="00002213 00006130b 00006130c 00009120 00016112 00032161 00038121 00044121 00047120 00048220 00053223 00058111 00084111 00095233 00099161 00105133 00111211 00131260 00142211 00166271 00170160 00172170 00196170 00220160 20200000 20210000 20270000 20580000 20710000 20970000 21040000 22080000 22280000"

if /bin/true; then
  # the two smallest for developing/testing/demonstration purposes
  DOCS="00038121 00058111"

  # use model of this fold ...
  TESTMODEL="00038121"
  # ... to predict coreference on this file
  TESTDOC="00038121"
fi

if /bin/false; then
  # four news documents (larger)
  DOCS="20970000 21040000 22080000 22280000"
  TESTMODEL="20970000"
  TESTDOC="21040000"
fi

OUTDIR=./tmp-cv/

CRSCORER="../reference-coreference-scorers/scorer.pl"

FASTTEXT=""
# set this to your fasttext installation/model/vector file, if word embedding features from fasttext should be used
# schubert
FASTTEXT="--fasttext=./fasttext-experiments/nobackup/fastText-0.1.0/fasttext,fasttext-experiments/nobackup/ftmodel-skipgram-defaults.bin,fasttext-experiments/nobackup/ftmodel-skipgram-defaults.vec"
# cobra
FASTTEXT="--fasttext=./fasttext-experiments/fastText-0.1.0/fasttext,fasttext-experiments/ftmodel-skipgram-defaults.bin,fasttext-experiments/ftmodel-skipgram-defaults.vec"

set -x

mkdir -p $OUTDIR
rm -f $OUTDIR/*

DOCARGS=""
for DOC in $DOCS; do
  DOCARGS="$DOCARGS --doc=$TBC_DIR/$DOC.xml --gold=$CRC_DIR/$DOC.xml"
done

echo "RUNNING SVC"

METHOD="sklearn.svm.LinearSVC(C=1.0,dual=False,verbose=1,class_weight='balanced',max_iter=5000)"
python3 crossvalidate_coref.py $FASTTEXT --outdir=$OUTDIR --method=$METHOD $DOCARGS

echo "PREDICTING COREFERENCE for document $TESTDOC using model learned for fold $TESTMODEL"

# predict coreference for testdoc with testmodel (must be part of crossvalidation)
python3 predictcoreference.py $FASTTEXT --in_doc=$TBC_DIR/$TESTDOC.xml --in_men=$CRC_DIR/$TESTDOC.xml \
  --model=$OUTDIR/$TESTMODEL.model.pk --out=$OUTDIR/$TESTDOC.predictcoreference.xml

echo "RUNNING REFERENCE SCORER"

# convert and score
python3 xml-to-conll.py $TBC_DIR/$TESTDOC.xml $CRC_DIR/$TESTDOC.xml $TESTDOC \
  $OUTDIR/$TESTDOC.gold.conll
python3 xml-to-conll.py $TBC_DIR/$TESTDOC.xml $OUTDIR/$TESTDOC.predictcoreference.xml $TESTDOC \
  $OUTDIR/$TESTDOC.predictcoreference.conll
$CRSCORER all $OUTDIR/$TESTDOC.gold.conll $OUTDIR/$TESTDOC.predictcoreference.conll |egrep -i "^(Coreference|BLANC):|identification of mentions|metric"

echo "RUNNING SVR"

METHOD="sklearn.svm.LinearSVR(C=1.0,verbose=1,loss='squared_epsilon_insensitive',dual=False,max_iter=25000)"
BOUNDS="--regbound=0.1"
python3 crossvalidate_coref.py $FASTTEXT --outdir=$OUTDIR $BOUNDS --method=$METHOD $DOCARGS 

# TODO implement predictcoreference.py for predicted mentions, by copying relevant code from crossvalidate_coref.py

