#!/usr/bin/python
# -*- coding: utf8 -*-

# Converting Marmara Turkish Coreference Corpus XML to CoNLL format.
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import collections

from knowlpcoref import *

def usage():
  print("usage: {} <document-file-in>.xml <coref-file-in>.xml <docname-for-scorer> <conll-file-out>.conll".format(sys.argv[0]))
  print("  <coref-file-in>.xml can be /dev/null, then document is just converted to conll")

def toConllInt(out, docname):
  def converter(line):
    ret = (str(line['sentence']), str(line['word']), line['sentenceno'], line['form'], line['coref'])
    return ret
  return toConll(docname, map(converter, out), formatting='space')

def main():
  documentfile = sys.argv[1]
  warn('reading {}'.format(documentfile))
  with open(documentfile, 'r') as f:
    document = getDocumentFromXML(f.read())

  coreffile = sys.argv[2]
  warn('reading {}'.format(coreffile))
  with open(coreffile, 'r') as f:
    corefxml = f.read()
    if len(corefxml) == 0:
      corefxml = "<?xml version='1.0' encoding='utf-8'?><coref><mentions /><chains /></coref>"
    annotation = getMentionsChainsFromXML(corefxml)

  docname = sys.argv[3]
  warn('docname {}'.format(docname))

  outfile = sys.argv[4]
  warn('will write to {}'.format(outfile))

  # pre-fill out list with sentence
  out = []
  address = {}
  for sentenceidx, sentence in enumerate(document):
    sentenceno = sentence['no']
    for word in sentence['words']:
      wordix = word['ix']
      form = word['text'].strip()
      address[ (sentenceno,wordix) ] = len(out)
      out.append({
        'document': documentfile,
        'sentence': sentenceidx,
        'sentenceno': sentenceno, # for debugging purposes
        'word': wordix,
        'form': form,
        'coref': '',
        'starting': [],
        'ending': [],
        'unit': [],
      })

  #with open(outfile, 'w') as of:
  #  of.write(toConll(out))

  # add coreferences

  # if multiple mentions start or end at the same place:
  # the longer ones start before and end after the shorter ones, for example:
  # (1(3(4
  # 4)
  # 3)
  # 1)
  # hence for ech record in out we first find all mentions beginning/ending here
  for cidx0, chain in enumerate(annotation['chains']):
    cidx = cidx0+1
    for mid in chain:
      mention = annotation['mentions'][mid]
      s, from_, to = mention['s'], int(mention['from']), int(mention['to'])
      startidx = address[(s,from_)]
      endidx = address[(s,to)]
      augmention = mention
      augmention.update({'chain': cidx, 'len': to-from_+1})
      if augmention['len'] == 1:
        assert(startidx == endidx)
        out[startidx]['unit'].append(augmention)
      else:
        out[startidx]['starting'].append(augmention)
        out[endidx]['ending'].append(augmention)
      #warn('for mention {} in chain {} startidx {} endidx {}:\n\tstarting = {}\n\tending={}\n\tunit={}'.format(
      #  mid, cidx, startidx, endidx, out[startidx]['starting'], out[endidx]['ending'], out[startidx]['unit']))

  for o in out:
    # starting sorted by length, descending
    starts = sorted(o['starting'], key = lambda x: -x['len'])
    # ending sorted by length, ascending
    ends = sorted(o['ending'], key = lambda x: x['len'])
    if len(ends) > 0 and len(starts) > 0:
      # we have ending and starting: first put ending, then unit, then starting
      corefstr = ''.join(
        [ '{})'.format(x['chain']) for x in ends ] +
        [ '({})'.format(x['chain']) for x in o['unit'] ] +
        [ '({}'.format(x['chain']) for x in starts ])
    elif len(ends) == 0:
      # we have no ending: first put starting and then unit
      corefstr = ''.join(
        [ '({}'.format(x['chain']) for x in starts ] +
        [ '({})'.format(x['chain']) for x in o['unit'] ])
    elif len(starts) == 0:
      # we have no starts: first put unit then end
      corefstr = ''.join(
        [ '({})'.format(x['chain']) for x in o['unit'] ] +
        [ '{})'.format(x['chain']) for x in ends ])
    if corefstr == '':
      o['coref'] = '-'
    else:
      o['coref'] = corefstr

  with open(outfile, 'w') as of:
    of.write(toConllInt(out, docname))

if __name__ == '__main__':
  if len(sys.argv) != 5:
    usage()
  else:
    main()
