#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Marmara Turkish Coreference Corpus Baseline: Mention Detection
# Copyright (c) 2016 Ferit Tunçer <ferit@cryptolab.net>
# Copyright (C) 2017 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, re, argparse
import xml.etree.ElementTree as etree
from xml.dom import minidom

import knowlpcoref

def debug(msg):
  if __debug__:
    sys.stderr.write(msg+'\n')

def main():
  interpretArguments()
  documentxml = parseInputXML(config['args'].inp)
  md = knowlpcoref.MentionDetector(config['args'].dummychains)
  mentionsxml = md.predictMentions(documentxml)
  writeOutputXML(config['args'].out, mentionsxml)

def parseInputXML(filename):
  parser = etree.XMLParser(encoding="UTF-8")
  tree = etree.parse(filename, parser=parser)
  root = tree.getroot()
  return root

def writeOutputXML(outfile, mentionsxml):
  with open(outfile, 'w') as of:
    of.write(prettify(mentionsxml))

def prettify(elem): # Returns pretty-printed XML
    rough_string = etree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="")

def interpretArguments():
  global config
  parser = argparse.ArgumentParser(
    description='Mention prediction baseline for Marmara Turkish Coreference Corpus')
  parser.add_argument('--inp', required=True, metavar='IN', action='store',
    help='Turkish Treebank Document XML-format input file.')
  parser.add_argument('--out', required=True, metavar='OUT', action='store',
    help='Turkish Coreference XML-format output file.')
  parser.add_argument('--dummychains', action='store_true',
    help='Create a dummy chain for each mentions (necessary for scoring mention-only files).')
  args = parser.parse_args(sys.argv[1:])
  config = { 'args': args }

if __name__ == '__main__':
  main()
