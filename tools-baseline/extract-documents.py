#!/usr/bin/python3
# -*- coding: utf8 -*-

# Converting METU-Sabanci Turkish Treebank fragments into Document XMLs.
# Copyright (C) 2015 Barış Gün Sürmeli
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# INSTRUCTIONS:
#
# This script uses as input the original files in the
# directory tb_corrected/ in the zip file tb_corrected.zip
# from the METU-Sabanci Turkish Treebank distribution.
#
# It first converts all files into a new directory
# rewritten_single_files/
# such that invalid XML parts (duplicate arguments, arguments
# with wrong encodings) are removed, which yields a set of
# wellformed XML documents in original encoding (windows-1254)
# corresponding to the original files.
#
# Then the script concatenates document parts into whole
# documents, modifies sentence IDs so that they are associated with
# the file name of the document XML and numbered from 1, and writes
# the resulting document XMLs (now consisting of several sentences)
# with the utf8 encoding (can be configured) into a new directory
# documents/

# Please setup paths below in the 'config' dictionary
# currently configured encoding: utf8
# alternative encoding: cp1254 / windows-1254


# Matching document parts to documents is done from the file names.
#
# The only files where the prefix does not determine the document are
# 1) 0000613019.xml 
# 2) 0000613020.xml and 0000613021.xml
# 3) 0000613022 and bigger numbered files.
# which are split into the above corresponding three documents.

import os, re, time, traceback
import xml.etree.ElementTree as ET

global config

config = {
  'inputdir' : 'tb_corrected',
  'outputenc' : 'utf-8',
  #'outputenc' : 'WINDOWS-1254',
  'correctdir' : 'rewritten_single_files',
  'concatdir' : 'documents',
}

for d in [config['concatdir'], config['correctdir']]:
  if not os.path.exists(d):
    print("creating output directory {}".format(d))
    os.makedirs(d)

def correct(fname):
    print("correcting", repr(fname))
    f = open(os.path.join(config['inputdir'], fname), 'rb')
    fileText = f.read().decode('windows-1254')
    fileText = re.sub(r'\ ORG_IG1=(.*?)\>', '>', fileText)
    fileText = re.sub(r'<w', r'<W', fileText)
    fileText = re.sub(r'w>', r'W>', fileText)
    strng = ""
    for i in fileText.split("\n"):
        t = re.findall(r'IG\=\'\[(.*?)\]\'', i)
        try:
            old = t[0]
            new = re.sub(r'\'', r'&#39;', t[0])
            old1 = "IG='[" + old + "]'"
            new1 = "IG='[" + new + "]'"
            fileText = fileText.replace(old1,new1)
        except:
            pass
        
    f.close()
    direc = config['correctdir']
    outname = os.path.join(direc, fname + "-CORRECTED.xml")
    splt = fileText.split("\n")
    for i in splt:
        if i == splt[0]:
            f = open(outname, 'w')
            f.write(i + "\n")  
        else:
            f = open(outname, 'a')
            f.write(i + "\n")
        f.close()
    try:
        mainTree = ET.parse(outname, ET.XMLParser(encoding='utf-8'))
        return mainTree
    except:
        print("in {} got {}".format(outname, traceback.format_exc()))
        raise


# find all files in dir
print("scanning for METU-Sabanci Treebank files in {}".format(config['inputdir']))
files = os.listdir(config['inputdir'])
print("files found in {}: {}".format(config['inputdir'], repr(files)))
# find all files that belong together (documents)
documents = set(map(lambda fname: fname[:8], files))
# collect individual files
collections = []
for docprefix in documents:
    r = docprefix + "(.*?)\.xml" 
    #print "regex ", repr(r)
    foundfiles = [ fname for fname in files if re.match(r, fname) ]
    if len(foundfiles) == 0:
      print("warning: ignoring prefix {}".format(docprefix))
      continue
    filenumbers = [ fname[8:-4] for fname in foundfiles ]
    filenumbers.sort(key=lambda x: int(x.strip('-')))
    #print "for prefix", docprefix, "found", repr(filenumbers)
    if docprefix == '00006130':
      assert(filenumbers[0:3] == ['19', '20', '21'])
      # split them into three parts
      allparts = [ docprefix + x for x in filenumbers ]
      # the first (19)
      collections.append( (docprefix+'a', allparts[0:1]) )
      # the second and third (20, 21)
      collections.append( (docprefix+'b', allparts[1:3]) )
      # the rest
      collections.append( (docprefix+'c', allparts[3:]) )
    else:
      # generic code
      collections.append( (docprefix, [ docprefix + x for x in filenumbers ]) )

for item in collections:
    prefix, files = item
    #print "prefix {} files {}".format(prefix, files)
    mainname = prefix

    fname = files[0]
    mainTree = correct(fname + '.xml')
    mainRoot = mainTree.getroot()
    a = 0
    for i in mainRoot.iter():
        if i.tag == "S":
            ind = i.attrib["No"]
            i.attrib["No"] = fname + "." + ind
            a = a + 1

    for fname in files[1:]:
            tree = correct(fname + '.xml')
                
            root = tree.getroot()            
            for el in root.iter():
                if el.tag == "S":
                    ind = el.attrib["No"]
                    el.attrib["No"] = fname + "." + ind
                    mainRoot.append(el)
                    a = a + 1
                    
    for f in mainRoot.iter("Set"):
        f.attrib["sentences"] = str(a)
    
    fName = mainname + ".xml"
    treee = ET.ElementTree(mainRoot)
    direc = config['concatdir']
    print("writing {} in {}".format(fName, direc))
    treee.write(os.path.join(direc, fName), encoding = config['outputenc'], xml_declaration=True )   
    
