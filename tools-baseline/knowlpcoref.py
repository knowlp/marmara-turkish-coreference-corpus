# python 3
# -*- coding: utf-8 -*-

# Marmara Turkish Coreference Corpus Library.
# Copyright (C) 2015-2017 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, collections, re, os, traceback, time, itertools
import subprocess

import xml.etree.ElementTree as ET
etree = ET

from sklearn.metrics.pairwise import cosine_similarity
import sklearn.decomposition
import gensim
import pickle

config = {
  'classical-features': True,
  'fasttext': None, # list of command/modelfile/vectorfile for fasttext
  'fasttext-use-pca': False,
  # cache for PCA computation result
  'fasttext_pca_cache_pickle_file': 'fasttext_pca_cache.pk',
  # cobra
  #'fasttext_pca_cache_pickle_file': '/mnt/vg01/lv01/home/ps/omsieve/fasttext_pca_cache.pk',
}

def message(msg):
  sys.stdout.write("{}\n".format(msg))
  sys.stdout.flush()

DEBUG = False

last = time.time()
def debug(msg):
  global last
  if DEBUG:
    sys.stderr.write('D {:6.3f} {}\n'.format(time.time()-last, msg))
    sys.stderr.flush()
    last = time.time()

def warn(msg):
  global last
  sys.stderr.write('W {:6.3f} {}\n'.format(time.time()-last, msg))
  sys.stderr.flush()
  last = time.time()

warnedOnce = set()
def warnOnce(msg, data=''):
  global warnedOnce
  if msg not in warnedOnce:
    sys.stderr.write("W: {} (warning only once){}".format(msg, data))
    sys.stderr.flush()
    warnedOnce.add(msg)


class Table:
  def __init__(self,colsep=' '):
    self.data = {}
    self.cols = set()
    self.rows = set()
    self.colsep = colsep
    self.colwidths = collections.defaultdict(int)

  def cell(self, row, col, content, align='right'):
    assert(isinstance(row,int))
    assert(isinstance(col,int))
    assert(isinstance(content,str))
    key = (row,col)
    self.cols.add(col)
    self.rows.add(row)
    self.data[key] = (content, align)
    self.colwidths[col] = max(self.colwidths[col], len(content))
    #warn('cell {} {} set to {} {}'.format(row, col, content, align))

  def __str__(self):
    # we only use used cols/rows
    realcols = sorted(self.cols)
    realrows = sorted(self.rows)
    out = ''
    for ridx, r in enumerate(realrows):
      if ridx > 0:
        out += '\n'
      first = True
      for c in realcols:
        if not first:
          out += self.colsep
        first = False
        if (r,c) in self.data:
          data, align = self.data[(r,c)]
        else:
          data, align = '', 'left'
        if align == 'left':
          out += data.ljust(self.colwidths[c])
        if align == 'right':
          out += data.rjust(self.colwidths[c])
    #warn('output of length {}'.format(len(out)))
    return out

def getMentionsChainsFromXML(xml):
  m = {}
  c = []
  root = ET.fromstring(xml)
  mnode = next(root.iter('mentions'))
  for ment in mnode:
    m[ment.attrib['id']] = {
      'from': int(ment.attrib['fromWordIX']),
      'to': int(ment.attrib['toWordIX']),
      's': ment.attrib['sentenceNo'],
      'text': ment.text,
    }
  cnode = next(root.iter('chains'))
  for chn in cnode:
    chain = []
    for mic in chn:
      chain.append(mic.attrib['mentionId'])
    c.append(frozenset(chain))
  return { 'mentions': m, 'chains': c }

rRel = re.compile(r'\[([0-9]+),([0-9]+),\(([A-Z.]+)\)\s*\]')
rIg = re.compile(r'\[\(1,"([^+]+)\+.*\)\]')
def getDocumentFromXML(xml):
  s = []
  root = ET.fromstring(xml)
  sentencenodes = next(root.iter('Set'))
  for snode in sentencenodes:
    words = []
    for wnode in snode:
      ix, ig, rel = int(wnode.attrib['IX']), wnode.attrib['IG'], wnode.attrib['REL']
      text = wnode.text
      assert(int(ix) - 1 == len(words))
      w = { 'ix': ix, 'ig': ig, 'text':text.strip(), 'rel': rel }
      m = rRel.match(rel)
      if not m:
        if rel != '[,( )]':
          warn("could not interpret rel {} for token {}".format(repr(rel), repr(w)))
        w.update({'rel_ix': None, 'rel_ig':None, 'rel_type':None})
      else:
        rel_ix, rel_ig, rel_type = m.groups()
        w.update({'rel_ix': int(rel_ix), 'rel_ig':int(rel_ig), 'rel_type':rel_type })
      m = rIg.match(ig)
      if not m:
        warn("could not interpret ig {} for token {}".format(repr(ig), repr(w)))
      else:
        w.update({'ig_lemma': m.group(1)})
      words.append(w)
    s.append({
      'no': snode.attrib['No'],
      'words': words
    })
  return s


def readCONLLRaw(text):
  'output in format { documentname : [ lines ], ... }'
  out = collections.OrderedDict()
  rBeginDoc = re.compile(r'^#\s*begin\s+document\s*\(?(.*)\)?\s*;?\s*$')
  rEndDoc = re.compile(r'^#\s*end\s+document\s*$')
  rSplit = re.compile(r'\s+')
  currentDoc = None
  currentDocLines = []
  for line in text.split('\n'):
    beg = rBeginDoc.findall(line)
    end = rEndDoc.findall(line)
    #warn('beg {}'.format(repr(beg)))
    if len(beg) > 0:
      if currentDoc is not None:
        raise Exception('encountered begin document {} within document {}'.format(beg[0], currentDoc))
      currentDoc = beg[0]
    elif len(end) > 0:
      if currentDoc is None:
        raise Exception('encountered end document outside of document')
      out[currentDoc] = currentDocLines
      currentDoc = None
      currentDocLines = []
    elif line.strip() == '':
      # handle empty lines gracefully
      pass
    else:
      fields = [ x for x in rSplit.split(line) if x != '' ]
      if len(fields) == 1:
        raise Exception('found only one field in CoNLL, expect at least 2 (for safety reasons)')
      if currentDoc is None:
        raise Exception('encountered data {} outside of #begin document / #end document'.format(repr(line)))
      currentDocLines.append(fields)
  return out

def interpretCoNLLRaw(lines, usecolumns, corefcolumn, onlyUser=False):
  '''
  ignores columns other than 'usecolumns' (a list/set of items)
  builds mentions/chains from column 'corefcolumn'
  this is for one document, output in format
    {
      'lines': [ '1\t3\t...\t(2)', '3\t...\t-', ... ],
      'mentions': { id: (linefrom,lineto), ... },
      'chains': { id: set([id1,id2]), ... }
      'none': set([line, ...]),
    }
  '''
  out = { 'lines':[], 'mentions':{}, 'chains':collections.defaultdict(set), 'none': set() }
  mentionId = 0
  openMentions = [] # (lineidx, chainID) tuples go here
  rCorefAtom = re.compile(r'^(\([0-9]+\)|\([0-9]+|[0-9]+\)).*$')
  for lineidx, line in enumerate(lines):
    thisline = [line[idx] for idx in usecolumns]
    out['lines'].append(thisline)
    coref = line[corefcolumn]
    if onlyUser:
      if coref[0] == '=':
        # ignore = and process
        coref = coref[1:]
      else:
        # ignore coref
        continue
    while len(coref) > 0 and coref != '-':
      m = rCorefAtom.match(coref)
      if m is None:
        raise Exception("uninterpretable coreference information '{}'".format(coref))
      #warn('m.groups {}'.format(m.groups()))
      match = m.groups()[0]
      # interpret match
      if match[0] == '(' and match[-1] == ')':
        # (XYZ)
        chain = int(match[1:-1])
        out['mentions'][mentionId] = (lineidx, lineidx)
        out['chains'][chain].add(mentionId)
        mentionId += 1
      elif match[0] == '(':
        # (XYZ
        chain = int(match[1:])
        openMentions.append( (lineidx, chain) )
      elif match[-1] == ')':
        # XYZ)
        chain = int(match[:-1])
        if len(openMentions) == 0:
          raise Exception('no open mentions but closing mention for chain {}'.format(chain))
        if openMentions[-1][1] != chain:
          raise Exception('closing mention for chain {} but currently innermost open mention is for chain {}'.format(
            chain, openMentions[-1][1]))
        fromline = openMentions[-1][0]
        out['mentions'][mentionId] = (fromline, lineidx)
        out['chains'][chain].add(mentionId)
        mentionId += 1
        openMentions = openMentions[:-1]
      else:
        raise Exception("unexpected case for match '{}'".format(match))

      # remove this coref element
      coref = coref[len(match):]
    if coref == '-':
      out['none'].add(lineidx)
  return out

def createCorefXML(mentDict, chains):
  '''
  mentDict: dictionary with key = mention ID and value
    {'toWordIX': 1, 'text': u'\u015eimdi', 'sentenceNo': '0000613019.1', 'wordIX': [1], 'fromWordIX': 1}
  chains: collection of collections of mention IDs
  '''
  #warn('createCorefXML: mentDict = {}'.format(repr(mentDict)))
  #warn('createCorefXML: chains = {}'.format(repr(chains)))

  # create XML
  newRoot = ET.Element("coref")
  
  # mentions in XML
  mentions = ET.SubElement(newRoot,"mentions")
  for mentionId, mdata in mentDict.items():
    mtag = ET.SubElement(mentions, "mention")
    mtag.attrib['id'] = str(mentionId)
    for src, tgt in [ ('s','sentenceNo'), ('from', 'fromWordIX'), ('to', 'toWordIX')]:
      mtag.attrib[tgt] = str(mdata[src])
    mtag.text = mdata['text']
  
  # chains in XML
  chainsNode = ET.SubElement(newRoot,"chains")
  for chain in chains:
    chainNode = ET.SubElement(chainsNode, "chain")
    debug("saving chain "+repr(chain))
    for mid in chain:
      if mid not in mentDict:
        warn("ignored mention {} in chain which does not exist in annotation!".format(mid))
        continue
      mNode = ET.SubElement(chainNode, "mention")
      mNode.attrib['mentionId'] = str(mid)
      # the following is only for debugging!
      mNode.text = mentDict[mid]['text']

  #'<?xml version="1.0" encoding="UTF-8" ?>\n'+ \
  xmlstr = ET.tostring(newRoot, encoding='utf-8', method='xml')
  return xmlstr.decode('utf-8')

def toConll(docname, lines, formatting='tabs'):
  '''
  formatting is either 'tabs' (default) or 'space'
  '''
  firstline = '#begin document {}'.format(docname)
  lastline = '#end document'
  if formatting == 'tabs':
    def formatLine(line):
      line = list(line)
      line[-1] = line[-1].strip('=') # remove forced tags
      return '\t'.join(line)
    conll = '\n'.join([firstline]+map(formatLine, lines)+[lastline]) + '\n'
    return conll
  else:
    t = Table()
    for lidx, line in enumerate(lines):
      for cidx, cell in enumerate(line):
        t.cell(lidx, cidx, cell)
    return firstline + '\n' + str(t) + '\n' + lastline + '\n'

def toTxt(docname, lines):
  def formatToken(tok):
    if tok in [',', '.', '!', '?']:
      return tok
    else:
      return ' '+tok.replace('_',' ')
  bodytext = ''.join(map(lambda x: formatToken(x[3]), lines))
  ret = 'Document {}:\n{}\n'.format(docname, bodytext.strip())
  #warn(repr(ret))
  return ret

def toXML(docname, interpreted):
  lines = interpreted['lines']
  mdict = {}
  COLUMN_SENTENCENO = 2
  COLUMN_WORDIX = 1
  COLUMN_WORD = 3
  for mid, mdata in interpreted['mentions'].items():
    fromline, toline = mdata
    mdict[mid] = {
      's': lines[fromline][COLUMN_SENTENCENO],
      'from': lines[fromline][COLUMN_WORDIX],
      'to': lines[toline][COLUMN_WORDIX],
      'text': ' '.join(map(lambda x: x[COLUMN_WORD], lines[fromline:toline+1]))
    }
    if lines[fromline][COLUMN_SENTENCENO] != lines[toline][COLUMN_SENTENCENO]:
      raise Exception('tried to create mention across sentences: from {} to {} (lines {})!'.format(
        fromline, toline, repr(lines[fromline:toline+1])))
  return createCorefXML(mdict, interpreted['chains'].values())

#
# for  coref baseline
#

class FasttextFeatureCreator:
  def __init__(self, ft_binary, ft_model, ft_vectorfile):
    self.ft_binary = ft_binary
    self.ft_model = ft_model
    self.ft_vectorfile = ft_vectorfile
    self.ft_popen = None
    self.cache = {}
    self.pcamethod = None
    # do not initialize anything expensive here, do it on demand

  def __del__(self):
    # kill the child process
    self._kill_process()

  def vectorize_all(self, input_tokens):
    return [ self.vectorize(x) for x in input_tokens ]

  def vectorize(self, token):
    #debug("vectorizing token {}".format(repr(token)))
    t = token.lower()
    try:
      return self.cache[t]
    except KeyError:
      vec = self._get_vector(t)
      self.cache[t] = vec
      return self.cache[t]

  def generate_features(self, m1d, m2d):
    def average(vectors):
      #debug("average got vectors")
      #for v in vectors:
      #  debug("  "+repr(v))
      l = float(len(vectors))
      ret = [ sum(x)/l for x in zip(*vectors) ]
      #debug("average returns "+repr(ret))
      return ret
    #debug("FasttextFeatureCreator.generate_features({},{})".format(repr(m1d), repr(m2d)))
    # v1/v2 = avg of word vectors of tokens in m1d['text']/m2d['text']
    # v1h/v2h = word vector of head of mention 1/2
    # ft_sim_tok feature: cosine similarity between v1 and v2
    # ft_sim_head feature: cosine similarity between v1h and v2h
    # ft_sim_th feature: cosine similarity between v1 and v2h
    # ft_sim_ht feature: cosine similarity between v1h and v2
    # ft_X_Y_Z: value of dimension Z of average of word vector X and Y as feature
    v1raw = self.vectorize_all(m1d['text'].split(' '))
    v1h = self.vectorize(m1d['head_token']['text'])
    #debug("vectors of {} are {} and {}".format(repr(m1d['text']), repr(v1raw), repr(v1h)))
    v1 = average(v1raw)
    #debug("average vector of {} is {}".format(repr(m1d['text']), repr(v1)))
    v2raw = self.vectorize_all(m2d['text'].split(' '))
    v2h = self.vectorize(m2d['head_token']['text'])
    #debug("vectors of {} are {} and {}".format(repr(m2d['text']), repr(v2raw), repr(v2h)))
    v2 = average(v2raw)
    #debug("average vector of {} is {}".format(repr(m2d['text']), repr(v2)))
    sim_tok = cosine_similarity([v1], [v2])
    sim_head = cosine_similarity([v1h], [v2h])
    #debug("cosine similarity between {} and {} is {}".format(m1d['head_token']['text'], m2d['head_token']['text'], sim_head))
    ret = { 'ft_sim_tok': sim_tok[0], 'ft_sim_head': sim_head[0], }
    # add single vectors as features
    dim = len(v1raw[0]) # vector dimension
    for name, v in [('t1', v1), ('h1', v1h), ('t2', v2), ('h2', v2h) ]:
      for d in range(0, int(dim)):
        ret['ft_'+name+'_'+str(d)] = v[d]
    return ret

  def _get_vector(self, token):
    global config
    assert(token == token.lower())
    # load file if necessary, lookup in file, raise exception if not found
    self._get_process().stdin.write(bytes(token+"\n", encoding='utf8'))
    self._get_process().stdin.flush()
    vec = self._get_process().stdout.readline().decode('utf8')
    #debug("received {}".format(repr(vec)))
    # do not convert first token (input word) and last token (newline)
    tup = tuple([ float(f) for f in vec.split(' ')[1:-1] ])
    if config['fasttext-use-pca']:
      return self._get_pca().transform([tup])[0]
    else:
      return tup

  def _get_pca(self):
    # get PCA result from cache or recompute it and store it to cache
    global config
    if not self.pcamethod:
      try:
        message("loading fasttext PCA from cache {}".format(config['fasttext_pca_cache_pickle_file']))
        with open(config['fasttext_pca_cache_pickle_file'], 'rb') as inf:
          self.pcamethod = pickle.load(inf)
      except:
        message("cache unsuccessful: recreating fasttext PCA")
        message("loading embeddings from {}".format(self.ft_vectorfile))
        embs = gensim.models.KeyedVectors.load_word2vec_format(self.ft_vectorfile, binary=False, unicode_errors='ignore', encoding='utf8')
        message("embeddings loaded,  shape (nWords, nDims): {}".format(embs.syn0.shape))
        debug("embedding for 'ben': {}".format(embs["ben"]))
        self.pcamethod = sklearn.decomposition.PCA(n_components=15)
        self.pcamethod.fit(embs.syn0)
        message("storing fasttext PCA to cache {}".format(config['fasttext_pca_cache_pickle_file']))
        with open(config['fasttext_pca_cache_pickle_file'], 'wb') as outf:
          pickle.dump(self.pcamethod, outf)
      message("Fasttext PCA: percentage of variance explained by components: {}".format(100.0*sum(self.pcamethod.explained_variance_ratio_)))
    return self.pcamethod

  def _get_process(self):
    # uses existing process or spawns new one if it does not exist
    if not self.ft_popen:
      self.ft_popen = subprocess.Popen("{} print-word-vectors {}".format(self.ft_binary, self.ft_model), shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    return self.ft_popen

  def _kill_process(self):
    if self.ft_popen:
      self.ft_popen.kill()
      self.ft_popen.wait()
    self.ft_popen = None

_ftfc = None
def get_ftfc():
  global _ftfc
  if not _ftfc:
    global config
    if config['fasttext']:
      _ftfc = FasttextFeatureCreator(*config['fasttext'])
  return _ftfc

def del_ftfc():
  global _ftfc
  if _ftfc:
    del(_ftfc)
  _ftfc = None


class MentionDetector:
  def __init__(self, dummychains=False):
    self.found_mentions_next = 1
    self.found_mentions = {}
    self.found_mentions_set = set()
    self.dummychains = dummychains

    self.output_xml = etree.Element('coref')
    self.mentions = etree.SubElement(self.output_xml, 'mentions')
    self.chains = etree.SubElement(self.output_xml, 'chains')

    self.rIGNamedEntity = re.compile(r'\[\(1,"[A-Z][^+]*\+Noun')

  def add_found_mention(self, text, stcno, fromwordix, towordix, dbg=None):
    # prevent duplicates
    uid = (stcno,fromwordix,towordix)
    if uid in self.found_mentions_set:
      return
    else:
      self.found_mentions_set.add(uid)
    # adds mention and returns ID
    mid = self.found_mentions_next
    self.found_mentions_next += 1

    mention = etree.SubElement(self.mentions, 'mention')
    mention.text = text
    mention.set("sentenceNo", stcno)
    mention.set("fromWordIX", fromwordix)
    mention.set("toWordIX", towordix)
    mention.set("id", str(mid))
    if dbg:
      debug("added mention {} {} {}".format(uid, text, dbg))
      #mention.set("DBG", dbg)

    if self.dummychains:
      a_chain = etree.SubElement(self.chains, 'chain')
      another_mention = etree.SubElement(a_chain, 'mention')
      another_mention.text = text
      another_mention.set("mentionId", str(mid))

    return mid

  def predictMentions(self, documentxml):
    def ig_is_named_entity(ig):
      ret = self.rIGNamedEntity.match(ig) is not None
      #debug("ig_is_named_entity({}) = {}".format(ig, ret))
      return ret

    def ig_is_determiner(ig_attrib):
      ret = re.search(r"Det", ig_attrib) is not None
      #debug("ig_is_determiner({}) = {}".format(ig_attrib, ret))
      return ret

    def is_noun(ig_attrib):
      return re.search(r"Noun", ig_attrib) is not None

    def is_modifier(ig_attrib):
      return re.search(r"MODIFIER", ig_attrib) is not None

    def is_capped(innertext):
      return innertext[0].isupper()

    # find capitalized common nouns appearing 2 or more times in document
    capped_common_nouns = collections.defaultdict(int)
    for item0 in documentxml.iter('S'):
      for item in item0.iter('W'):
        innertext = "".join(item.itertext()).strip()
        if is_capped(innertext) and is_noun(item.attrib['IG']):
          capped_common_nouns[innertext] += 1
    capped_common_nouns_appearing_2_or_more = set([ noun
      for noun, count in capped_common_nouns.items() if count > 1 ])
    #print("capped 2 or more: "+repr(capped_common_nouns_appearing_2_or_more))

    mention_count = 0
    noun_phrases = []

    for item0 in documentxml.iter('S'):
      xmlitems = list(item0.iter('W'))
      # for each token that is a pronoun (lemma is Pronoun, IG is no Determiner), take it
      # for each token that is a named entity (capitalized lemma), take it
      # for each token that is a capitalized common noun appearing two or more times, take it
      for item in xmlitems:
        innertext = "".join(item.itertext()).strip()
        mlemma = rIg.match(item.attrib['IG'])
        lemma = mlemma.group(1)
        pronoun = is_pronoun(lemma, innertext)
        det = ig_is_determiner(item.attrib['IG'])
        named_entity = ig_is_named_entity(item.attrib['IG'])
        capped_c_noun = item in capped_common_nouns_appearing_2_or_more

        if (pronoun and not det) or named_entity or capped_c_noun:
          ix = item.attrib["IX"]
          mid = self.add_found_mention(innertext, item0.attrib["No"], ix, ix, dbg='PRON/NE/C2+')
      
      # represent all elements depending on some other element
      #
      # key = idx in xmlitems, value = idx in xmlitems
      head_of = collections.defaultdict(set)
      # extract one-step from XML
      for idx, item in enumerate(xmlitems):
        # idx = str(IX)-1
        mr = rRel.match(item.attrib['REL'])
        if not mr:
          continue
        points_to, ig_idx, rel = mr.groups()
        #debug("at item {} text {} ig {} rel {} {}".format(ix, item.text, item.attrib['IG'], points_to, rel))
        points_to = int(points_to)-1
        head_of[points_to].add(idx)
      # transitively saturate dependencies
      #debug("head_of before saturation: "+repr(head_of))
      changed = True
      while changed:
        changed = False
        new = []
        for idx1 in range(0, len(xmlitems)):
          for idx2 in head_of[idx1]:
            for idx3 in head_of[idx2]:
              if idx3 not in head_of[idx1]:
                new.append( (idx1, idx3) )
        if len(new) > 0:
          for idx1, idx3 in new:
            head_of[idx1].add(idx3)
          changed = True
      debug("head_of after saturation: "+repr(head_of))

      # for each token that is a Noun
      #   find the largest span to the left and right having this noun as Head,
      #   take it
      for idx, item in enumerate(xmlitems):
        if not (is_noun(item.attrib['IG']) and not is_modifier(item.attrib['REL'])):
          continue

        debug("at item {} text {} ig {}".format(idx, item.text, item.attrib['IG']))

        # search to left
        start = idx
        for sstart in range(idx-1, -1, -1):
          if sstart in head_of[idx]:
            start = sstart
          else:
            break
        # search to right
        end = idx
        for send in range(idx+1, len(xmlitems)):
          if send in head_of[idx]:
            end = send
          else:
            break
        debug("   start {} end {}".format(start, end))

        # create mention from start to end
        mtext = ' '.join([ ''.join(tok.itertext()).strip()
                            for tok in xmlitems[start:end+1] ])
        mid = self.add_found_mention(mtext, item0.attrib["No"], str(start+1), str(end+1), dbg='noun phrase')

    return self.output_xml


def debug_mention(tokens, md):
  debug("m {} {}".format(md['id'], md))
  for t in tokens[md['token_from']:md['token_to']+1]:
    debug("  "+repr(t))

def debug_mentionpair(tokens, pmd, md):
  debug_mention(tokens, pmd)
  debug_mention(tokens, md)

def importDocPlusCoref(docfile, crfile, limit=None):
  # limit can be used to partially load a file (to speedup development)
  stc = getDocumentFromXML(open(docfile, "r").read())
  m_ch = getMentionsChainsFromXML(open(crfile, "r").read())
  # flat representation:
  # 'tokens' -> flattened sentences
  # inside all mentions: add 'token_from', 'token_to' -> index into 'tokens'
  out = { 'tokens': [], 'mentions': m_ch['mentions'], 'chains':m_ch['chains'] }
  if limit:
    # remove all except first limit mentions
    out['mentions'] = dict( list(out['mentions'].items())[:limit] )
    # remove from chains
    remainingmentions = set(out['mentions'].keys())
    debug("limited mentions to {}".format(repr(remainingmentions)))
    out['chains'] = list(map(lambda c: c & remainingmentions, out['chains']))
    debug("limited chains to {}".format(repr(out['chains'])))

  # build tokens
  tid = 0
  stc_by_no, token_by_s_ix = {}, {}
  for s in stc:
    stc_by_no[s['no']] = s
    for w in s['words']:
      token_by_s_ix[ (s['no'], w['ix']) ] = w
      w['idx'] = tid
      tid += 1
      out['tokens'].append(w)
  out['token_by_s_ix'] = token_by_s_ix
  return out

def enrichMentions(tokens, token_by_s_ix, mentions):
  for mid, m in mentions.items():
    m['id'] = mid
    m['token_from'] = token_by_s_ix[ (m['s'], m['from']) ]['idx']
    m['token_to'] = token_by_s_ix[ (m['s'], m['to']) ]['idx']
    enrichMention(m, tokens)
  markMentionTypes(tokens, mentions)

def markMentionTypes(tokens, mentions):
  # collect capitalized non-initial tokens as proper nouns
  def simplify(t):
    text = t['text'].strip("'")
    if "'" in text:
      text = text.split("'")[0]
    return text
  pns = set(map(simplify, filter(lambda t: t['text'][0].isupper() and t['ix'] != 1, tokens)))
  #debug("found proper nouns {}".format(pns))
  for m in mentions.values():
    mtype = 'np'
    if m['token_from'] == m['token_to'] and is_pronoun(tokens[m['token_from']]['ig_lemma'], tokens[m['token_from']]['text']):
      mtype = 'pro'
    if any([m['head_token']['text'].startswith(pn) for pn in pns]):
      mtype = 'pn'
    m['mtype'] = mtype

# from Kilicaslan2009
pronouns = set('ben sen o biz siz onlar bura ora şura kendim kendin kendi kendimiz kendiniz kendileri birbirimiz birbiriniz birbirileri'.split(' '))
pronouns.update(set('bu on şu'.split(' ')))
def is_pronoun(lemma, text):
  return lemma.lower() in pronouns

def enrichMention(md, tokens):
  # add data fields to mention:
  # * head
  # * extent
  #debug("enrichMention")
  #debug_mention(tokens, md)
  try:
    if md['token_from'] == md['token_to']:
      md['head_at'] = md['token_from']
      md['head_token'] = tokens[md['token_from']]
    else:
      # find all tokens that have dependency to outside
      htokens = list(filter(
        lambda x: x['rel_ix'] and x['rel_ix'] not in range(tokens[md['token_from']]['ix'], tokens[md['token_to']]['ix']+1),
        tokens[md['token_from']:md['token_to']+1]))
      if len(htokens) > 1:
        debug("found more than one head token in mention '{}': {}, using last one".format(
          md['text'], ', '.join(map(lambda x: x['text'], htokens))))
        #debug(repr(htokens))
        md['head_token'] = htokens[-1]
        md['head_at'] = htokens[-1]['idx']
      else:
        md['head_at'] = md['token_to']
        md['head_token'] = tokens[md['token_to']]
    #if md.head_at:
    #  # modifiers before head
    #  mods = filter(lambda t: t.rel_type in ['MODIFIER', 'POSSESSOR'], tokens[md['token_from']:md.head_at+1])
    #  #warn("before: {}".format(repr(before)))
    #  md['modifiers'] = mods
  except:
    warn("Exception in enrichMention: "+traceback.format_exc())
    debug_mention(tokens, md)
    raise

def mention_overlaps_mention(pm, m):
  if pm['token_from'] <= m['token_from'] and pm['token_to'] >= m['token_from']:
    return True
  if pm['token_from'] >= m['token_from'] and pm['token_from'] <= m['token_to']:
    return True
  return False

MAXDIST=100
def check_mention_mention_tuple(pm, m, pm_idx, m_idx):
  # returns false if tuple should not be considered for training and classification
  # pm = predecessor candidate mention
  # m = current candidate mention
  # idx = indices in sorted mention list of pm and m
  
  # do not build tuple for pairs with mention index distance > MAXDIST
  if (m_idx - pm_idx) > MAXDIST:
    return False
  # do not build tuple if mention is part of other mention
  if mention_overlaps_mention(pm,m):
    return False
  # ignore links from non-pronouns back to pronouns
  if pm['mtype'] == 'pro' and m['mtype'] != 'pro':
    #debug("ignoring (pronoun/non-pronoun) mention pair {}/{}".format(pm['text'], m['text']))
    return False
  return True

def create_mention_mention_tuples(tokens, mentions):
  # returns a list (mention1id, mention2id) of mention-mention links to predict
  outmm = []
  # sort mentions by occurrence
  flatmentions = sorted(mentions.values(), key=lambda m: 10000*m['token_from']+m['token_to'])
  #debug("flatmen"+repr(flatmentions))
  #
  # according to Bengtson and Roth 2008
  # we consider all pairs that are not violating pronoun/nonpronoun rule
  # 
  # go through mentions
  for at_idx in range(0, len(flatmentions)):
    m = flatmentions[at_idx]
    # go through all preceding mentions
    for pred_idx in range(at_idx-1, -1, -1):
      pm = flatmentions[pred_idx]
      if check_mention_mention_tuple(pm, m, pred_idx, at_idx):
        outmm.append( (pm['id'], m['id']) )
  debug("created {} mention-mention candidates with maxdist {}".format(len(outmm), MAXDIST))
  return outmm

def create_mention_mention_tuples_gold(tokens, mentions, chains):
  # returns a tuple: mm, mmg
  # mm = list (mention1id, mention2id) of mention-mention links to predict
  # mmg = list of integers (1 if mentions in same chain, 0 otherwise)
  outmm, outmmg = [], []
  #debug("MEN"+repr(mentions))
  # sort mentions by occurance
  flatmentions = sorted(mentions.values(), key=lambda m: 10000*m['token_from']+m['token_to'])
  #debug("flatmen"+repr(flatmentions))
  #
  # according to Bengtson and Roth 2008
  # for each mention m we select from m's equivalence class the closest preceding mention a 
  # and present (a,m) as a positive training example
  #
  # we generate negative examples for all mentions that precede m and are not in the same class
  #
  # go through mentions
  for at_idx in range(0, len(flatmentions)):
    m = flatmentions[at_idx]
    found_positive = False
    # find equivalence class
    chain = [ c for c in chains if m['id'] in c ]
    if len(chain) > 0:
      chain = chain[0]
    else:
      chain = frozenset()
    # go through all preceding mentions pm but start at the one closest to m
    for pred_idx in range(at_idx-1, -1, -1):
      #debug("checking at_idx {} pred_idx {}".format(at_idx, pred_idx))
      pm = flatmentions[pred_idx]
      if check_mention_mention_tuple(pm, m, pred_idx, at_idx):
        if pm['id'] in chain:
          # positive example
          if not found_positive:
            found_positive = True
            outmm.append( (pm['id'], m['id']) )
            outmmg.append(1)
        else:
          # negative example
          outmm.append( (pm['id'], m['id']) )
          outmmg.append(0)
  debug("created {} mention-mention gold tuples with maxdist {}".format(len(outmm), MAXDIST))
  return outmm, outmmg

def create_mention_mention_features(tokens, mentions, candidates):
  # candidates = list of tuples (mention1id, mention2id)
  # returns a list of feature dictionaries, one for each tuple in candidates
  def tf(value):
    if value:
      return 1
    else:
      return 0
  def create(mpair):
    m1, m2 = mpair
    m1d, m2d = mentions[m1], mentions[m2]
    #debug("create_mention_mention_features")
    #debug_mentionpair(tokens, m1d, m2d)

    #
    # t = mention type features
    #
    t = {
      '1type':m1d['mtype'], '2type':m2d['mtype'],
      'pro': tf(m1d['mtype']=='pro' and m2d['mtype']=='pro'),
      'np': tf(m1d['mtype']=='np' and m2d['mtype']=='np'),
      'pn': tf(m1d['mtype']=='pn' and m2d['mtype']=='pn')
    }

    #
    # f = mention match features
    #
    # alias/acronym
    if '_' in m1d['text']:
      m1last = m1d['text'].split('_')[-1]
    else:
      m1last = m1d['text']
    if '_' in m2d['text']:
      m2last = m2d['text'].split('_')[-1]
    else:
      m2last = m2d['text']
    acronym = lambda m: ''.join(map(lambda t: t['text'][0], tokens[m['token_from']:m['token_to']+1]))
    hsub12 = m1d['head_token']['text'] in m2d['head_token']['text']
    hsub21 = m2d['head_token']['text'] in m1d['head_token']['text']
    hlsub12 = m1d['head_token']['ig_lemma'] in m2d['head_token']['ig_lemma']
    hlsub21 = m2d['head_token']['ig_lemma'] in m1d['head_token']['ig_lemma']
    acro1 = acronym(m1d).lower() == m2d['text'].lower()
    acro2 = acronym(m2d).lower() == m1d['text'].lower()
    #lcoverlap = len( set(m1d['text'].strip().split(' ')) & set(m2d['text'].strip().split(' ')) )
    #dist = m2d['token_from'] - m1d['token_from']
    f = {
      # head match
      'hmatch':tf(m1d['head_token']['text'] == m2d['head_token']['text']),
      # head lemma match
      'hlmatch':tf(m1d['head_token']['ig_lemma'] == m2d['head_token']['ig_lemma']),
      # head substring
      'hsub12':tf(hsub12), 'hsub21':tf(hsub21), 'hsub':tf(hsub12 or hsub21),
      # head lemma substring
      'hlsub12':tf(hlsub12), 'hlsub21':tf(hlsub21), 'hlsub':tf(hlsub12 or hlsub21),
      # alias/acronym
      'last':tf(m1last == m2last),
      'acro1': tf(acro1), 'acro2': tf(acro2), 'acro': tf(acro1 or acro2),
      #'ovr': lcoverlap, # feature decreases scores
      #'dist': dist, # feature decreases scores
    }
    # combine features
    combo = {}
    if config['classical-features']:
      combo.update(dict([
          ( tk+'.'+fk, str(tv)+'.'+str(fv) ) for tk, tv in t.items() for fk, fv in f.items() ]))
      combo.update(t)
      combo.update(f)
      if False:
        # noun phrase role features (causes performance drop)
        relh1, relh2 = 'none', 'none'
        if m1d['head_token']['rel_type'] is not None:
          relh1 = m1d['head_token']['rel_type']
        if m2d['head_token']['rel_type'] is not None:
          relh2 = m2d['head_token']['rel_type']
        for idx, rel in [ (1,relh1), (2,relh2) ]:
          combo['relh_{}'.format(idx)] = rel
    if get_ftfc():
      combo.update(get_ftfc().generate_features(m1d, m2d))
    #debug("feature for mentionpair:")
    #for k, v in combo.items():
    #  debug("  {} : {}".format(k, v))
    return combo
  return [create(x) for x in candidates]


#def discretize(vector, threshold):
#  def discr(x):
#    if x < threshold:
#      return 0
#    else:
#      return 1
#  return [ discr(x) for x in vector ]

def consistent(chain, mentions):
  # return False if chain contains mentions that are contained in other mentions in same chain, otherwise False
  for c1, c2 in itertools.combinations(chain, 2):
    if mention_overlaps_mention(mentions[c1], mentions[c2]):
      return False
  return True

def build_chains_regression(mmcandidates, pred, mentions, regbound):
  # pred contains something representing possibility of linking mention into previous cluster
  # for each mention find the highest pred that is above regbound and where clusters are compatible and join (if exists)
  bysecond = collections.defaultdict(list)
  # first group mmcandidates and pred according to second mentions
  for m1m2, p in zip(mmcandidates, pred):
    m1, m2 = m1m2
    # only add those above bound
    if p > regbound:
      bysecond[m2].append( (m1, p) )
  # XXX: we are processing mentions in some order, we should process in welldefined order
  chains = set()
  # now we have mentions as keys and for each a list of previous mentions and pred
  for m2, mlist in bysecond.items():
    # sort list by regression value (think: estimated probability), descending
    mlist = sorted(mlist, key=lambda mp: -mp[1])
    # take first valid match and go to next mention
    for m1, p in mlist:
      if integrateEquivalenceIntoChainsIfConsistent(mentions, chains, m1, m2):
        # leave mlist loop -> go to next m2 ("best link" is first compatible link due to sorting)
        break
  return chains

def build_chains_classification(mmcandidates, pred, mentions):
  # pred contains 0 or 1 for each mmcandidate: 1 means it is a link
  # if pred is 1, join mention clusters
  chains = set()
  for idx, val in enumerate(pred):
    if val == 0:
      continue
    m1, m2 = mmcandidates[idx]
    assert(m1 != m2)
    integrateEquivalenceIntoChainsIfConsistent(mentions, chains, m1, m2)
  return chains

def integrateEquivalenceIntoChainsIfConsistent(mentions, chains, m1, m2):
  # join m1 and m2
  # return True if successful
  cnew = set([m1, m2])
  match = [ c for c in chains if m1 in c or m2 in c ] # all chains matching m1 or m2
  for c in match:
    cnew |= c

  if consistent(cnew, mentions):
    # remove old, add new
    for c in match:
      chains.remove(c)
    chains.add(frozenset(cnew))
    return True
  else:
    return False

# example output
#====== TOTALS =======
#Identification of Mentions: Recall: (999 / 1184) 84.37% Precision: (999 / 999) 100%     F1: 91.52%
#--------------------------------------------------------------------------
#Coreference: Recall: (646 / 862) 74.94% Precision: (646 / 735) 87.89%   F1: 80.9%
#--------------------------------------------------------------------------

rMetricScoreLine = re.compile(r'^METRIC (.*):$')
rIMentionsScoreLine = re.compile(r'^Identification of Mentions:\s+Recall: .*\) ([0-9.]+)%\s+Precision: .*\) ([0-9.]+)%\s+F1: ([0-9.-]+)%$')
rCorefF1ScoreLine = re.compile(r'^Coreference:\s+Recall: .*\) ([0-9.]+)%\s+Precision: .*\) ([0-9.]+)%\s+F1: ([0-9.-]+)%$')
rBLANCLine = re.compile(r'^BLANC:\s+Recall: .*\) ([0-9.]+)%\s+Precision: .*\) ([0-9.]+)%\s+F1: ([0-9.-]+)%$')
def parseScorerLogContent(logcontent):
  out = {}
  metric = 'null' # if we only score one metric, there is no "metric" line in the output
  totals = False
  for line in logcontent.decode('utf8').splitlines(): #('\n'):
    mmo = rMetricScoreLine.match(line)
    if mmo:
      metric = mmo.groups(0)[0].lower()
      total = False
      continue
    if metric is None:
      continue
    if '= TOTALS =' in line:
      totals = True
    if not totals:
      continue
    #warn('SCORER:'+line)
    #warn('mo = '+repr(mo))
    #warn('mo.groups = '+repr(mo.groups()))
    mo = rIMentionsScoreLine.match(line)
    if mo:
      out[metric+'.m.r'] = float(mo.groups()[0])
      out[metric+'.m.p'] = float(mo.groups()[1])
      out[metric+'.m.f1'] = float(mo.groups()[2])
    mo = rCorefF1ScoreLine.match(line)
    if mo:
      out[metric+'.c.r'] = float(mo.groups()[0])
      out[metric+'.c.p'] = float(mo.groups()[1])
      out[metric+'.c.f1'] = float(mo.groups()[2])
    mo = rBLANCLine.match(line)
    if mo:
      out['blanc.c.r'] = float(mo.groups()[0])
      out['blanc.c.p'] = float(mo.groups()[1])
      out['blanc.c.f1'] = float(mo.groups()[2])
    
  return out

