#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Marmara Turkish Coreference Corpus Baseline: Coreference Resolution
# Copyright (c) 2016 Ferit Tunçer <ferit@cryptolab.net>
# Copyright (C) 2017 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, re, argparse
import pickle
import sklearn.svm
from sklearn import metrics
from sklearn.feature_extraction import DictVectorizer
import knowlpcoref

def debug(msg):
  sys.stderr.write('D: '+msg+'\n')

def main():
  interpretArguments()
  # load model
  #debug("loading model")
  with open(config['modelfile'], 'rb') as inf:
    pkl = pickle.load(inf)
  #vec = DictVectorizer()
  #model = sklearn.svm.LinearSVC(C=1.0, verbose=1, class_weight='balanced', max_iter=100)
  vec, model = pkl['vec'], pkl['model']
  # load document
  #debug("loading document")
  doc = knowlpcoref.importDocPlusCoref(config['htm'], config['xml'])
  knowlpcoref.enrichMentions(doc['tokens'], doc['token_by_s_ix'], doc['mentions'])
  mmcandidates = knowlpcoref.create_mention_mention_tuples(doc['tokens'], doc['mentions'])
  debug("creating features from {} mention pairs".format(len(mmcandidates)))
  mmf = knowlpcoref.create_mention_mention_features(doc['tokens'], doc['mentions'], mmcandidates)
  # predict
  debug("transforming {} features".format(len(mmf)))
  doc_x = vec.transform(mmf)
  #debug("predicting")
  doc_ypred = model.predict(doc_x)
  # build chains
  #debug("building chains")
  chains = knowlpcoref.build_chains_classification(mmcandidates, doc_ypred, doc['mentions'])
  # output
  with open(config['out'], 'w') as of:
    of.write(knowlpcoref.createCorefXML(doc['mentions'], chains))

def interpretArguments():
  global config
  parser = argparse.ArgumentParser(
    description='Coreference prediction baseline for Marmara Turkish Coreference Corpus')
  parser.add_argument('--model', required=True, metavar='IN', action='store',
    help='File that contains the (previously trained) coreference prediction model.')
  parser.add_argument('--in_doc', required=True, metavar='IN', action='store',
    help='Turkish Treebank Document XML-format input file.')
  parser.add_argument('--regbound', metavar='IN', action='append',
    help='Give regression best-link bounds (if omitted, use classification: use all-joins).')
  parser.add_argument('--noclassical', action='store_true',
    help='Do not use classical features (in that case you need to use fasttext for any predictions)')
  parser.add_argument('--fasttext', required=False, metavar='IN', action='store', default='',
    help='Command, model, vectorfile from fasttext for predicting word vectors. Format: executable,modelfile,vectorfile')
  parser.add_argument('--fasttextpca', action='store_true',
    help='Use PCA to reduce fasttext ectors to 15 dimensions.')
  parser.add_argument('--in_men', required=True, metavar='IN', action='store',
    help='Turkish Treebank Coreference XML-format input file (mentions will be used, chains will be ignored).')
  parser.add_argument('--out', required=True, metavar='OUT', action='store',
    help='Turkish Coreference XML-format output file.')
  args = parser.parse_args(sys.argv[1:])

  htm, xml = args.in_doc, args.in_men
  m = re.search(r".*[^0-9]?([0-9a-c]{8,9})\.(?:xml|htm)", htm)
  if not m:
    raise Exception("got malformed input file name {}".format(htm))
  doc = m.group(1)
  config = {
    'modelfile': args.model, 'out': args.out,
    'htm': htm, 'xml': xml, 'doc': doc }
  if args.regbound and len(args.regbound) > 0:
    config['regbounds'] = list(map(float, config['regbounds']))
    config['regression'] = True
  if args.fasttext != '':
    knowlpcoref.config['fasttext'] = args.fasttext.split(',')
  if args.fasttextpca:
    knowlpcoref.config['fasttext-use-pca'] = True
  if args.noclassical:
    knowlpcoref.config['classical-features'] = False

if __name__ == '__main__':
  main()
