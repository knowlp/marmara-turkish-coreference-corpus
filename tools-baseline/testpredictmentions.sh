#!/bin/bash

TBC_DIR="../documents"
CRC_DIR="../gold"

# a single document
TESTDOCS="00016112"
# all documents
TESTDOCS="00002213 00006130b 00006130c 00009120 00016112 00032161 00038121 00044121 00047120 00048220 00053223 00058111 00084111 00095233 00099161 00105133 00111211 00131260 00142211 00166271 00170160 00172170 00196170 00220160 20200000 20210000 20270000 20580000 20710000 20970000 21040000 22080000 22280000"
#TESTDOCS="00111211"

# output directory
TMP=./out-md/

CRSCORER="../reference-coreference-scorers/scorer.pl"

mkdir -p $TMP
rm -f $TMP/*

# create GOLD CONLL
for DOC in $TESTDOCS; do
  python3 ./xml-to-conll.py $TBC_DIR/$DOC.xml $CRC_DIR/$DOC.xml $DOC $TMP/$DOC.gold.conll
done
# one conll for all
cat $TMP/*.gold.conll >$TMP/gold.conll

# predict mentions and convert to CONLL
for DOC in $TESTDOCS; do
  python3 ./predictmentions.py --dummychains --inp=$TBC_DIR/$DOC.xml --out=$TMP/$DOC.mention.out.xml
  python3 ./xml-to-conll.py $TBC_DIR/$DOC.xml $TMP/$DOC.mention.out.xml $DOC $TMP/$DOC.mention.out.conll
done

# one conll for all
cat $TMP/*.mention.out.conll >$TMP/mention.out.conll

# score
{
  # score with MUC
  for DOC in $TESTDOCS; do
    echo -n "$DOC "
    $CRSCORER muc $TMP/gold.conll $TMP/mention.out.conll $DOC |grep -i "identification of mentions"
  done
  echo -n "ALL "
  $CRSCORER muc $TMP/gold.conll $TMP/mention.out.conll |grep -i "identification of mentions"
}
